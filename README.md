# Super Mileage Backend

This is significantly more complicated than the frontend. 

Chances are, most people will be developing the frontend using a central environment like this guide helps set up. This means there is one server (called either "dev" or "staging"), and every client connects to it. This is how it will work in it's final (production) version anyways. This means that not everyone has to set one up, and everyone has the same version.

Ask the current lead of the Computer & Electric team for the IP/Hostname of the API.

If, for some reason, this central environment does not exist anymore, this will be the backup.

The only reason why you would need to set this up is if:

1. You are developing the backend.
2. The central server no longer exists.
3. The scope of this project has gotten out of hand.

Just some terminology definition:

1. Controller: Actually does stuff (accessing database, etc).
2. Router: Routes the user to the controller.
3. Entity: Basically an object you can get out of the database. Contains all of the necessary columns to fetch data from the database.

# 1 - Getting started

## Option A (Faster) - Install Docker

### Install Docker & Compose

---

Windows:


If you're on Windows Home (Pro can use this), sorry. You're out of luck. Do option B or use a central development environment. 

If you're on Windows Pro or higher, follow these steps.

https://docs.docker.com/docker-for-windows/install/

Next, install Docker Compose

https://docs.docker.com/compose/install/

---

Linux (Ubuntu):

Remove all past deprecated versions of Docker: 

`sudo apt-get remove docker docker-engine docker.io containerd runc`

Update repositories:


`sudo apt-get update`

Install needed packages:


`sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common` 

Add Docker key:

`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

Hopefully you're on an x86/amd64 machine. If you don't know what that means, then you are.

Add Docker apt repository:

`sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" && sudo apt-get update`

Install Docker engine:

`sudo apt-get install docker-ce docker-ce-cli containerd.io`

Install Docker Compose:

`sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

Change Compose permissions:

`sudo chmod +x /usr/local/bin/docker-compose`

Alternatively, do EVERYTHING above in one big ol command line: https://pastebin.com/DpuK8kuB

And you're done. Wasn't that SO much easier than Windows?

## Option B (Better if you know what you're doing)

This is for if you already have a MySQL server up and going.

You also have to know a little about the command line to do this one.

Import data from `tools/dump.sql` into a new database named `supermileage`

Done. As long as your MySQL service is open on port 3306 then you're good. If you don't know what this means, then you're good.

# 2 - Start the environment

First, follow all of the instructions in step 1 from https://gitlab.com/SpBills/super-mileage-frontend

Next, run 

`npm start`

## If you followed option A:

Finally, run 

`docker-compose up`

# 3 - Point the Frontend to this backend.

In the Frontend (web app) we set up a .env file. Inside of this .env file, find 

`HOST_URL=currentcentralip` or something of the like.

Change this line to:

`HOST_URL=localhost`

Restart your application, and now the web app will use your local version.