import Controller from './controller';
import { getConnection } from 'typeorm';
import { Request, Response } from 'express';
import QuoteEntity from "../entities/quote.entity"

/**
 * A controller that defines all of the behavior whenever accessing properties on a Car.
 */
class QuoteController implements Controller {
    create(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    async read(_req: Request, res: Response): Promise<void> {
        const data = await getConnection().getRepository(QuoteEntity).find({ take: 500 });

        res.json(data);
    }
    update(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    delete(): Promise<void> {
        throw new Error("Method not implemented.");
    }

    async readOne(req: Request, res: Response): Promise<void> {
        const data = await getConnection().getRepository(QuoteEntity).findOne({ where: { id: req.params.id }});

        res.json(data);
    }

    async readRandom(_req: Request, res: Response): Promise<void> {
        let ids = await getConnection().getRepository(QuoteEntity).find({ select: ["id"] });
        const data = await getConnection().getRepository(QuoteEntity).findOne({ where: { id: Math.floor((Math.random() * <number> ids?.length!) + 1) }});

        res.json(data);
    }
}

export default QuoteController