import { Request, Response } from 'express';

/**
 * An interface that defines all behaviors of an HTTP controller.
 * These controllers, once implemented, will provide a good outline for what every controller should do.
 * If you are NOT going to use one of these, just throw a new error inside of the path that you are not planning on using.
 * 
 * Example:
 * delete(): Promise<void> {
        throw new Error("Method not implemented.");
    }
 */
interface Controller {
    create(req: Request, res: Response): Promise<void>;

    read(req: Request, res: Response): Promise<void>;

    update(req: Request, res: Response): Promise<void>;

    delete(req: Request, res: Response): Promise<void>;
}

export default Controller;