import { Entity, PrimaryColumn, Column } from "typeorm";

@Entity()
class dt_quotes {
    @PrimaryColumn()
    id: Number

    @Column()
    quote: String

    @Column()
    length: Number

    @Column()
    time_created: Date
}

export default dt_quotes;