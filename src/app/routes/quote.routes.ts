import Router from './router';
import QuoteController from '../controllers/quote.controller';
import Express from "express"

/**
 * For now, nothing needs to be defined in here.
 * Whenever the scope of this goes out of the original Router,
 * for instance getting individual entities,
 * that is whenever you need to start adding functions to this.
 */
class QuoteRoutes extends Router<QuoteController> {
    public registerRoutes(router: Express.Router) {
        router.route(this.routePath)
            .post(this.controller.create)
            .get(this.controller.read)
            .put(this.controller.update)
            .delete(this.controller.delete);

        router.route(`${this.routePath}/random`)
            .get(this.controller.readRandom)

        router.route(`${this.routePath}/:id`)
            .get(this.controller.readOne)
    }
}

export default QuoteRoutes