/**
 * Defines a generic HTTP error.
 */
export default class HTTPError {
    public message: string;
    public status: number;
    public error: Error;

    /**
     * @param message Custom message you would like to send to the user on error.
     * @param status The error status.
     * @param error The error object, straight from Express (usually).
     */
    constructor(message: string, status: number, error: Error) {
        this.message = message;
        this.status = status;
        this.error = error;
    }
}