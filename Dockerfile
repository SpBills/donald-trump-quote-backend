FROM node

COPY . .

# Install typescript so we can compile for prod.
RUN npm i -g typescript

RUN tsc

RUN npm remove typescript

WORKDIR /dist

COPY ./.env.production ./.env
COPY ./node_modules ./node_modules

RUN "ls"

EXPOSE 8090
CMD ["node", "./app.js"]